<?PHP
	
	/*	#MapLabBZH La carte des FabLabs & autres lieux de dissémination des usages numériques en Bretagne
		=================================================================================================
			Auteur	Guillaume Rouan | @grouan
			Blog	http://guillaume-rouan.net/blog/
		-------------------------------------------------------------------------------------------------
			Titre	maplabbzh_fonctions.inc.php
			Sources 	https://grouan.github.io/bzh_fablab/
			Licence	CC-BY
			Made with CSS Framework Bulma + Font Awesome
		-------------------------------------------------------------------------------------------------
	*/

// --- Calcul de la distance entre 2 points (LAT, LON, ALT) --------------------------
	function distance($lat1, $lon1, $lat2, $lon2) {
		$r = 6366; //rayon de la terre
		$lat1 = deg2rad($lat1);
		$lat2 = deg2rad($lat2);
		$lon1 = deg2rad($lon1);
		$lon2 = deg2rad($lon2);
 
		//recuperation altitude en km
		/*$alt1 = $alt1/1000;
		$alt2 = $alt2/1000;*/
		$alt1 = 0;
		$alt2 = 0;
 
		//calcul précis
		$dp= 2 * asin(sqrt(pow (sin(($lat1-$lat2)/2) , 2) + cos($lat1)*cos($lat2)* pow( sin(($lon1-$lon2)/2) , 2)));
 
		//sortie en km
		$d = $dp * $r;
 
		//Pythagore a dit que :
		 $distance = sqrt(pow($d,2)+pow($alt2-$alt1,2)); // h
 
		return $distance;
	}
	
// --- RSS Feed ---------------------------------------------------------------------

	function getPosts($feed_url) {
	
	    $content = file_get_contents($feed_url);
	    $x = new SimpleXmlElement($content);
		  /*
	        $feed = strip_tags (
		preg_replace('`<img[^>]*>.+?</img>`is','',
		$x )
	        );
	        */
	
	    echo '<ul class="blog-posts clearfix">';
	    $i=1;
	    foreach($x->channel->item as $entry) {
	        $date = strtotime($entry->pubDate);
	        $dateFR = date("d/m/Y", $date);
	        echo '<li class="blog-post">';
	        echo '<a href="'.$entry->link.'" title="'.$entry->title.'" target="_blank" class="font-black">' . $entry->title . '</a> <span class="icon is-small"><i class="fa fa-clock-o"></i></span> '.$dateFR.' ';
	        if ($entry->description != '') { echo '<span class="no-mobile" style="color:#333;"> | '.$entry->description.'</span>'; }
	        echo '</li>';
	        $i++;
	        if($i >= 11) // Nombre d'actualités // 5 actus => 6
	        break;        
	    }
	    echo "</ul>";
	}

// --- ... -------------------

?>