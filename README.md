# Bienvenue sur #MapLabBZH<br />la Carte des FabLabs & Tiers-Lieux de Bretagne
<img src="https://raw.githubusercontent.com/grouan/bzh_fablab/master/img/wiki/maplabbzh_logo_250x226.png" alt="Carte des FabLabs & Tiers-Lieux de Bretagne — Guillaume Rouan — #MapLabBZH" />

Ce projet, initié en 2015, est né d'une initiative ouverte visant à formaliser une documentation inexistante jusqu’à lors et à améliorer la visibilité des espaces / services / projets qui œuvrent d’une même dynamique sur le territoire breton. **Elle recense les FabLabs, Tiers-Lieux & autres lieux de dissémination des usages numériques en Bretagne.** Collaborative, libre et Open Source, cette carte est aujourd'hui en perpétuelle amélioration et pleinement accessible, sur la base  du crowdsourcing. Ainsi, vous pouvez librement y contribuer, l’utiliser, l’intégrer à vos sites web, la modifier, l’adapter, la remixer…

Consultez :
- [la carte](https://grouan.fr/maplabbzh/maplabbzh_carte) 🗺️
- [la liste](https://grouan.fr/maplabbzh/maplabbzh_liste) :open_file_folder:

> Pour tout savoir sur ce projet, rendez-vous sur [le wiki](https://github.com/grouan/bzh_fablab/wiki) 🚀

> Pour visualiser ou les sources, téléchargez [le dépôt bzh_fablab](https://github.com/grouan/bzh_fablab) 📥

> Parce que votre participation est indispensable : [contribuez](http://guillaume-rouan.net/maplabbzh/maplabbzh_contribuer.php) :+1: !

<hr />

Utilisez le hashtag **#MapLabBZH** pour en parler 💬

<a href="http://bit.ly/DataGouvFabLabs" target="_blank"><img src="https://raw.githubusercontent.com/grouan/bzh_fablab/master/img/wiki/datagouv_logo.png" alt="DataGouv : Carte des FabLabs & Tiers-Lieux de Bretagne" /></a> <a href="http://bit.ly/GeoBretagneFabLabs" target="_blank"><img src="https://raw.githubusercontent.com/grouan/bzh_fablab/master/img/wiki/geobretagne_logo.png" alt="GéoBretagne : Carte des FabLabs & Tiers-Lieux de Bretagne" /></a> <a href="http://bit.ly/MapLabsBZH_RFF" target="_blank"><img src="https://raw.githubusercontent.com/grouan/bzh_fablab/master/img/wiki/reseauff_logo.png" alt="Réseau Français des FabLabs : Carte des FabLabs & Tiers-Lieux de Bretagne" /></a> 

<hr />

🔓 Code source & données sous <a href="https://www.eupl.eu" target="_blank">licence EUPL</a><br />
🔓 UI Liste & UI Carte sous <a href="https://creativecommons.org/licenses/by-sa/2.0/fr/" target="_blank">licence CC BY-SA</a>
